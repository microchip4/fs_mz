//Reference
//https://www.aidanmocke.com/blog/2018/04/10/delays/

#ifndef _DELAY_H
#define _DELAY_H

#define SYS_FREQ 200000000 // Running at 200MHz

void delay_us(unsigned int us);
void delay_ms(int ms);
void delay_s(int s);

#endif /* _DELAY_H */

//DOM-IGNORE-END

/*******************************************************************************
 End of File
 */