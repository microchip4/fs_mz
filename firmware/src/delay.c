#include "delay.h"
#include "system/common/sys_module.h"   // SYS function prototypes

void delay_us(unsigned int us)
{
    // Convert microseconds us into how many clock ticks it will take
    us *= SYS_FREQ / 1000000 / 2; // Core Timer updates every 2 ticks
    _CP0_SET_COUNT(0); // Set Core Timer count to 0
    while (us > _CP0_GET_COUNT()); // Wait until Core Timer count reaches the number we calculated earlier
}

void delay_ms(int ms){
    delay_us(ms * 1000);
}

void delay_s(int s){
    delay_ms(s * 1000);
}