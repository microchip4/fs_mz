// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "system/common/sys_module.h"   // SYS function prototypes
#include "app.h"
#include "diskio.h"
#include "ff.h"
#include "delay.h"

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

uint8_t *spi_write_read(SPI_MODULE_ID index, uint8_t *write_buffer, size_t write_num,
                      uint8_t *read_buffer, uint32_t read_num);

//Compile:  make build
//Program:  make program
//          make download

void error_indicator(int n){
    int i;
    for (i = 0; i < 2 * n; i++)
    {
        BSP_LEDToggle(BSP_RGB_LED_GREEN);
        delay_ms(500);
    }
}

int main ( void )
{
    /* Initialize all MPLAB Harmony modules, including application(s). */
    SYS_Initialize ( NULL );
    /*SD*/
    #define SZ_TBL 1000  
    FATFS fs;			/* File system object */
    FIL fil;        /* File object */
    FRESULT sdcheck;
    static UINT nbytes;
    /*Mount*/
    BSP_LEDOff(BSP_RGB_LED_RED);
    BSP_LEDOff(BSP_RGB_LED_GREEN);
    BSP_LEDOff(BSP_RGB_LED_BLUE);
    int i;
    for (i = 1; i < 4; i++){
        int j;
        for (j = 0; j < 2; j++)
        {
            BSP_LEDToggle(i);
            delay_ms(100);
        }
    }
    
    for(;;){
        BSP_LEDOn(BSP_LED_1);
        sdcheck = f_mount(&fs,"",1);
        if(!sdcheck) break;
    }
    BSP_LEDOff(BSP_LED_1);
    BSP_LEDOn(BSP_LED_2);
    
    static uint8_t * wr_buffer = "Texto de pruebas!";

    sdcheck = f_open(&fil,"BRV64.TXT",FA_CREATE_ALWAYS | FA_READ | FA_WRITE);
    //error_indicator(sdcheck);
    sdcheck = f_write(&fil,wr_buffer,strlen(wr_buffer),&nbytes);
    //error_indicator(sdcheck);
    sdcheck = f_close(&fil);
    //error_indicator(sdcheck);

    BSP_LEDOff(BSP_LED_2);
    while(1){
        BSP_LEDOn(BSP_LED_3);
    }
    /* Execution should not come here during normal operation */
    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

